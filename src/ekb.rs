use crate::nonce;
use openssl::symm::{encrypt, Cipher};
use rand::Rng;

pub struct EKB {
    id: u32,
    depth: u8,
    signature: [u8; 24],
    chain: [u8; 32],
    root: [u8; 16],
    iv: [u8; 8],
    content_id: [u8; 20],
    kek: [u8; 8],
    data_key: [u8; 8],
    nonce: nonce::Nonce,
}

pub fn new_ekb() -> Result<EKB, Box<dyn std::error::Error>> {
    let ekb: EKB = match nonce::new_nonce() {
        Ok(nonce) => {
            EKB {
                // data key is created with the create_key method
                data_key: [0; 8],
                id: 0x26422642,
                chain: [
                    0x25, 0x45, 0x06, 0x4d, 0xea, 0xca, 0x14, 0xf9, 0x96, 0xbd, 0xc8, 0xa4, 0x06,
                    0xc2, 0x2b, 0x81, 0x49, 0xba, 0xf0, 0xdf, 0x26, 0x9d, 0xb7, 0x1d, 0x49, 0xba,
                    0xf0, 0xdf, 0x26, 0x9d, 0xb7, 0x1d,
                ],
                depth: 9,
                signature: [
                    0xe8, 0xef, 0x73, 0x45, 0x8d, 0x5b, 0x8b, 0xf8, 0xe8, 0xef, 0x73, 0x45, 0x8d,
                    0x5b, 0x8b, 0xf8, 0x38, 0x5b, 0x49, 0x36, 0x7b, 0x42, 0x0c, 0x58,
                ],
                root: [
                    0x13, 0x37, 0x13, 0x37, 0x13, 0x37, 0x13, 0x37, 0x13, 0x37, 0x13, 0x37, 0x13,
                    0x37, 0x13, 0x37,
                ],
                iv: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
                content_id: [
                    0x01, 0x0F, 0x50, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x48, 0xA2, 0x8D, 0x3E,
                    0x1A, 0x3B, 0x0C, 0x44, 0xAF, 0x2f, 0xa0,
                ],
                kek: [0x14, 0xe3, 0x83, 0x4e, 0xe2, 0xd3, 0xcc, 0xa5],
                nonce,
            }
        }
        Err(e) => return Err(e),
    };
    return Ok(ekb);
}

impl EKB {
    pub fn create_key(&self) -> Result<[u8; 8], Box<dyn std::error::Error>> {
        let mut data_key: [u8; 8] = [0; 8];
        for i in 0..8 {
            data_key[i] = rand::thread_rng().gen();
        }
        return Ok(data_key);
    }

    pub fn key_16_to_24(&self) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        let b = [&self.root[0..16], &self.root[0..8]].concat();
        return Ok(b);
    }

    // calculates the session key
    pub fn retail_mac(&self) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        // generate IV

        // DES-ECB encrypt the root key
        let des_cipher = Cipher::des_cbc();
        let iv = match encrypt(des_cipher, &self.root[0..8], None, &self.nonce.host) {
            Ok(iv) => iv,
            Err(e) => return Err(Box::new(e)),
        };

        // use Triple DES-CBC (EDE3) to generate the key
        let triple_des_cipher = Cipher::des_ede3_cbc();
        let key_24 = match self.key_16_to_24() {
            Ok(key) => key,
            Err(e) => return Err(e),
        };
        let session_key = match encrypt(triple_des_cipher, &key_24, Some(&iv), &self.nonce.dev) {
            Ok(session_key_crypt) => session_key_crypt,
            Err(e) => return Err(Box::new(e)),
        };

        return Ok(session_key);
    }
}
