#[derive(Debug)]
pub enum LibUSBError {
    Success = 0,
    IO = -1,
    InvalidParam = -2,
    Access = -3,
    NoDevice = -4,
    NotFound = -5,
    Busy = -6,
    Timeout = -7,
    Overflow = -8,
    Pipe = -9,
    Interrupted = -10,
    NoMem = -11,
    NotSupported = -12,
    Other = -99,
}

impl LibUSBError {
    pub fn from_i32(value: i32) -> LibUSBError {
        match value {
            0 => LibUSBError::Success,
            -1 => LibUSBError::IO,
            -2 => LibUSBError::InvalidParam,
            -3 => LibUSBError::Access,
            -4 => LibUSBError::NoDevice,
            -5 => LibUSBError::NotFound,
            -6 => LibUSBError::Busy,
            -7 => LibUSBError::Timeout,
            -8 => LibUSBError::Overflow,
            -9 => LibUSBError::Pipe,
            -10 => LibUSBError::Interrupted,
            -11 => LibUSBError::NoMem,
            -12 => LibUSBError::NotSupported,
            _ => LibUSBError::Other,
        }
    }
}

impl std::error::Error for LibUSBError {}

impl std::fmt::Display for LibUSBError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            LibUSBError::Success => write!(f, "Success (no error)"),
            LibUSBError::IO => write!(f, "Input/output error"),
            LibUSBError::InvalidParam => write!(f, "Invalid parameter"),
            LibUSBError::Access => write!(f, "Access denied (insufficient permissions)"),
            LibUSBError::NoDevice => write!(f, "No such device (possibly disconnected)"),
            LibUSBError::NotFound => write!(f, "Entity not found"),
            LibUSBError::Busy => write!(f, "Resource busy"),
            LibUSBError::Timeout => write!(f, "Operation timeout"),
            LibUSBError::Overflow => write!(f, "Overflow"),
            LibUSBError::Interrupted => write!(f, "System call interrupted"),
            LibUSBError::NoMem => write!(f, "Insufficient memory"),
            LibUSBError::NotSupported => write!(f, "Operation not supported on this platform"),
            LibUSBError::Pipe => write!(f, "Pipe error"),
            LibUSBError::Other => write!(f, "unknown error"),
        }
    }
}

#[derive(Debug)]
pub struct NetMDNoDeviceError;
impl std::error::Error for NetMDNoDeviceError {}
impl std::fmt::Display for NetMDNoDeviceError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "No NetMD device was detected at requested index, or at all"
        )
    }
}

#[derive(Debug)]
pub struct NetMDNoSyncResponse;
impl std::error::Error for NetMDNoSyncResponse {}
impl std::fmt::Display for NetMDNoSyncResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "No response from device to sync request")
    }
}

#[derive(Debug)]
pub struct NetMDInvalidPollResponse;
impl std::error::Error for NetMDInvalidPollResponse {}
impl std::fmt::Display for NetMDInvalidPollResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Invalid response from device to poll request")
    }
}

#[derive(Debug)]
pub struct NetMDInvalidControlResponse;
impl std::error::Error for NetMDInvalidControlResponse {}
impl std::fmt::Display for NetMDInvalidControlResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Invalid response from device to poll request")
    }
}

#[derive(Debug)]
pub struct NetMDDataPending;
impl std::error::Error for NetMDDataPending {}
impl std::fmt::Display for NetMDDataPending {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "NetMD device is not ready for new commands, there is pending data on the bus"
        )
    }
}

#[derive(Debug)]
pub struct NetMDShortResponse;
impl std::error::Error for NetMDShortResponse {}
impl std::fmt::Display for NetMDShortResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "NetMD device responded with a message which was too short"
        )
    }
}

#[derive(Debug)]
pub struct NetMDDeviceNotOpen;
impl std::error::Error for NetMDDeviceNotOpen {}
impl std::fmt::Display for NetMDDeviceNotOpen {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "NetMD device has not been opened")
    }
}

#[derive(Debug)]
pub struct NetMDControlMessageRejected;
impl std::error::Error for NetMDControlMessageRejected {}
impl std::fmt::Display for NetMDControlMessageRejected {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "NetMD device rejected the control message")
    }
}

#[derive(Debug)]
pub struct NetMDControlMessageNotImplemented;
impl std::error::Error for NetMDControlMessageNotImplemented {}
impl std::fmt::Display for NetMDControlMessageNotImplemented {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "NetMD device reported command is not implemented")
    }
}

#[derive(Debug)]
pub struct NetMDResponseTimedOut;
impl std::error::Error for NetMDResponseTimedOut {}
impl std::fmt::Display for NetMDResponseTimedOut {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Waiting for NetMD device to respond timed out")
    }
}

#[derive(Debug)]
pub struct NetMDInterimTimedOut;
impl std::error::Error for NetMDInterimTimedOut {}
impl std::fmt::Display for NetMDInterimTimedOut {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "Waiting for NetMD device to return data for interim response"
        )
    }
}

#[derive(Debug)]
pub struct NetMDInvalidTimeStamp;
impl std::error::Error for NetMDInvalidTimeStamp {}
impl std::fmt::Display for NetMDInvalidTimeStamp {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "could not parse NetMD timestamp bytes into duration")
    }
}
