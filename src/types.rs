#[derive(Debug, Clone)]
pub struct DiscCapacity {
    pub recorded: u64,
    pub available: u64,
    pub total: u64,
}

#[derive(Debug, Clone)]
pub struct RecordingParameters {
    pub encoding: Encoding,
    pub channels: Channels,
}

impl std::fmt::Display for RecordingParameters {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} {}", self.encoding, self.channels)
    }
}

#[derive(Debug, Clone)]
pub struct PollResponse {
    pub length: u8,
    pub command: u8,
}

#[repr(u8)]
#[derive(Debug, Clone)]
pub enum Encoding {
    SP = 0x90,
    LP2 = 0x92,
    LP4 = 0x93,
    Unknown,
}

impl Encoding {
    pub fn from_u8(value: u8) -> Encoding {
        match value {
            0x90 => Encoding::SP,
            0x92 => Encoding::LP2,
            0x93 => Encoding::LP4,
            _ => Encoding::Unknown,
        }
    }
}

impl std::fmt::Display for Encoding {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Encoding::SP => write!(f, "SP"),
            Encoding::LP2 => write!(f, "LP2"),
            Encoding::LP4 => write!(f, "LP4"),
            Encoding::Unknown => write!(f, "Unknown encoding"),
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone)]
pub enum Channels {
    Stereo = 0x00,
    Mono = 0x01,
    Unknown = 0xff,
}

impl Channels {
    pub fn from_u8(value: u8) -> Channels {
        match value {
            0x00 => Channels::Stereo,
            0x01 => Channels::Mono,
            _ => Channels::Unknown,
        }
    }
}

impl std::fmt::Display for Channels {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Channels::Mono => write!(f, "Mono"),
            Channels::Stereo => write!(f, "Stereo"),
            Channels::Unknown => write!(f, "Unknown channel format"),
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone)]
pub enum ControlStatus {
    Control = 0x00,
    Status = 0x01,
    SpecificInquiry = 0x02,
    Notify = 0x03,
    GeneralInquiry = 0x04,
    NotImplemented = 0x08,
    Accepted = 0x09,
    Rejected = 0x0a,
    InTransition = 0x0b, //🏳️‍⚧️
    Implemented = 0x0c,
    Changed = 0x0d,
    Interim = 0x0f,
    Unknown = 0xff,
}

impl ControlStatus {
    pub fn from_u8(value: u8) -> ControlStatus {
        match value {
            0x00 => ControlStatus::Control,
            0x01 => ControlStatus::Status,
            0x02 => ControlStatus::SpecificInquiry,
            0x03 => ControlStatus::Notify,
            0x04 => ControlStatus::GeneralInquiry,
            0x08 => ControlStatus::NotImplemented,
            0x09 => ControlStatus::Accepted,
            0x0a => ControlStatus::Rejected,
            0x0b => ControlStatus::InTransition,
            0x0c => ControlStatus::Implemented,
            0x0d => ControlStatus::Changed,
            0x0f => ControlStatus::Interim,
            _ => ControlStatus::Unknown,
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone)]
pub enum TrackFlag {
    Protected = 0x03,
    Unprotected = 0x00,
    Unknown = 0xff,
}

impl TrackFlag {
    pub fn from_u8(value: u8) -> TrackFlag {
        match value {
            0x03 => TrackFlag::Protected,
            0x00 => TrackFlag::Unprotected,
            _ => TrackFlag::Unknown,
        }
    }
}

impl std::fmt::Display for TrackFlag {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            TrackFlag::Protected => write!(f, "Protected"),
            TrackFlag::Unprotected => write!(f, "Unprotected"),
            TrackFlag::Unknown => write!(f, "Unknown protection mode"),
        }
    }
}

#[repr(u8)]
#[derive(Debug, Clone)]
pub enum Status {
    NoDisc = 0x80,
    DiscLoaded = 0x40,
    Unknown = 0xff,
}

impl Status {
    pub fn from_u8(value: u8) -> Status {
        match value {
            0x80 => Status::NoDisc,
            0x40 => Status::DiscLoaded,
            _ => Status::Unknown,
        }
    }
}

impl std::fmt::Display for Status {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Status::NoDisc => write!(f, "No disc"),
            Status::DiscLoaded => write!(f, "Disc inserted"),
            Status::Unknown => write!(f, "Unknown device status"),
        }
    }
}
