#![warn(missing_docs)]
//! Control NetMD MiniDisc equipment.
//!
//! Provides abstraction of the NetMD protocol and USB device handling to enable control of NetMD-enabled USB MiniDisc devices, primarily recorders.

extern crate libc;
extern crate libusb_sys as ffi;

mod device;
mod ekb;
mod errors;
mod handshakes;
mod nonce;
mod types;
mod utils;
use device::*;

use ffi::{
    libusb_claim_interface, libusb_close, libusb_context, libusb_device, libusb_device_descriptor,
    libusb_device_handle, libusb_free_device_list, libusb_get_bus_number,
    libusb_get_device_address, libusb_get_device_descriptor, libusb_get_device_list, libusb_init,
    libusb_open, LIBUSB_ENDPOINT_IN, LIBUSB_ENDPOINT_OUT, LIBUSB_RECIPIENT_INTERFACE,
    LIBUSB_REQUEST_TYPE_VENDOR,
};
use ffi::{libusb_control_transfer, libusb_exit};

use crate::errors::*;
use crate::utils::hex_timestamp_to_duration;
use log::{debug, error};
//use rusb::{request_type, DeviceList, Direction, Recipient, RequestType};

use libc::{c_int, c_uchar};
use std::mem;
use std::ptr;
use std::slice;
use std::thread::sleep;
use std::time::Duration;
use types::*;
use utils::u8_array_to_u16;

/// Represents a NetMD device.
///
/// Use the `new_netmd` function in this crate to create a new instance
pub struct NetMD {
    /// The index of this NetMD device, for selecting between multiple connected NetMD devices
    pub index: usize,
    /// A `NetMDDevice` representing the details about the matched supported NetMD device, including model details
    pub device: NetMDDevice,
    /// A `String` containing the detected device's model information
    pub device_name: String,
    /// An `ekb::EKB` instance used for encryption operations, such as sending audio data.
    pub ekb: ekb::EKB,
    /// The libusb context used when connecting to the device
    libusb_context: *mut libusb_context,
    /// The libusb device handle used when connecting to the device
    usb_device_handle: *mut libusb_device_handle,
}

/// Creates a new `NetMD` instance by detecting and connecting to a known NetMD device
pub fn new_netmd(index: usize) -> Result<NetMD, Box<dyn std::error::Error>> {
    let md_ekb = match ekb::new_ekb() {
        Ok(ekb) => ekb,
        Err(e) => {
            error!("failed to generate EKB: {:?}", e);
            return Err(e);
        }
    };

    let mut context: *mut libusb_context = unsafe { mem::uninitialized() };

    match unsafe { libusb_init(&mut context) } {
        0 => (),
        e => panic!("libusb_init: {}", LibUSBError::from_i32(e)),
    };

    // iterate over devices and find NetMD known hardware
    let mut device_list: *const *mut libusb_device = unsafe { mem::uninitialized() };

    let len = unsafe { libusb_get_device_list(context, &mut device_list) };

    if len < 0 {
        error!(
            "libusb_get_device_list: {}",
            LibUSBError::from_i32(len as c_int)
        );
        return Err(Box::new(LibUSBError::from_i32(len as c_int)));
    }

    let devs = unsafe { slice::from_raw_parts(device_list, len as usize) };
    let mut detected_device: Option<NetMDDevice> = None;
    let mut detected_device_handle: Option<*mut libusb_device_handle> = None;
    let mut device_index: usize = 0;

    // iterate over devices
    'devloop: for dev in devs {
        let mut descriptor: libusb_device_descriptor = unsafe { mem::uninitialized() };
        let mut handle: *mut libusb_device_handle = ptr::null_mut();

        let bus = unsafe { libusb_get_bus_number(*dev) };
        let address = unsafe { libusb_get_device_address(*dev) };

        // does the device have a descriptor? we need this to work out if it's a NetMD device and talk to it
        match unsafe { libusb_get_device_descriptor(*dev, &mut descriptor) } {
            0 => {
                let response = unsafe { libusb_open(*dev, &mut handle) };
                if response < 0 {
                    debug!(
                        "couldn't open USB device during device probing: {}, skipping",
                        LibUSBError::from_i32(response)
                    );
                    continue 'devloop;
                }
                // we have the device open and a descriptor
                debug!(
                    "checking device bus {:03}, device {:03}, vendor {:04x}, product {:04x}",
                    bus, address, descriptor.idVendor, descriptor.idProduct
                );
                for device in device::NETMD_DEVICES {
                    if descriptor.idProduct == device.product_id
                        && descriptor.idVendor == device.vendor_id
                    {
                        // we have found a known device, check the index
                        debug!(
                            "found known NetMD device {} for index {}, checking index",
                            device.name, index
                        );
                        if device_index == index {
                            let claim_result = unsafe { libusb_claim_interface(handle, 0x0) };
                            if claim_result < 0 {
                                error!("failed to claim interface on device at bus {:03}, device {:03}, interface 0: {}", bus, address, LibUSBError::from_i32(claim_result));
                            } else {
                                debug!("chose NetMD device {} for index {}", device.name, index);
                                detected_device = Some(device.clone());
                                detected_device_handle = Some(handle);
                                break 'devloop;
                            }
                        } else {
                            device_index += 1;
                        }
                    }
                }
                // close device
                unsafe { libusb_close(handle) }
            }
            _ => debug!(
                "failed to get descriptor for device at bus {:03}, device {:03}",
                bus, address
            ),
        };
    }

    // free device list
    unsafe { libusb_free_device_list(device_list, 1) };

    let netmd = match detected_device_handle {
        Some(detected_device_handle) => match detected_device {
            Some(detected_device) => NetMD {
                device: detected_device.clone(),
                usb_device_handle: detected_device_handle,
                device_name: detected_device.clone().name.to_string(),
                libusb_context: context,
                ekb: md_ekb,
                index,
            },
            None => return Err(Box::new(errors::NetMDNoDeviceError {})),
        },
        None => return Err(Box::new(errors::NetMDNoDeviceError {})),
    };
    Ok(netmd)
}

// drop the libusb context when the NetMD instance is dropped
impl Drop for NetMD {
    fn drop(&mut self) {
        if !self.libusb_context.is_null() {
            debug!("releasing netmd context");
            unsafe { libusb_exit(self.libusb_context) };
        }
    }
}

impl NetMD {
    /// Ensures the previous operations are complete, prevents crashes on the SHARP IM-DR410/IM-DR420 and the Sony MZ-N420D
    pub fn wait(&self) -> Result<(), Box<dyn std::error::Error>> {
        let mut control_buffer: [u8; 4] = [0; 4];
        // 10 retries
        for _tries in 1..10 {
            let response = unsafe {
                libusb_control_transfer(
                    self.usb_device_handle,
                    LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE,
                    0x01,
                    0,
                    0,
                    control_buffer.as_mut_ptr() as *mut c_uchar,
                    control_buffer.len() as u16,
                    2000,
                )
            };
            if response >= 0 {
                debug!(
                    "received NetMD wait response with size {}: {:02X?}",
                    response, control_buffer
                );
                if response == 4 {
                    if control_buffer == [0x00, 0x00, 0x00, 0x00] {
                        return Ok(());
                    }
                }
            // don't error on timeout, because we want to sleep and then try again
            } else if response != LibUSBError::Timeout as i32 {
                return Err(Box::new(LibUSBError::from_i32(response)));
            }
            // sleep
            sleep(Duration::from_millis(100))
        }
        return Err(Box::new(NetMDResponseTimedOut {}));
    }

    /// Send a request to the NetMD unit.
    ///
    /// First polls the NetMD device to ensure no data is pending, and if there is no pending data to be read, will send a new request to the unit.
    pub fn transmit(
        &self,
        check: Vec<u8>,
        payload: Vec<u8>,
    ) -> Result<i32, Box<dyn std::error::Error>> {
        let control_payload: Vec<u8> = [vec![0x00], check.clone(), payload.clone()].concat();
        let control_payload_buffer: &[u8] = &control_payload;
        debug!(
            "-> NetMD transmit() with check {:02X?}, payload {:02X?}, combined payload {:02X?}",
            check, payload, control_payload
        );
        loop {
            debug!("polling NetMD device to ensure no data is waiting before sending command");
            match self.poll(1) {
                Ok(poll_response) => {
                    if poll_response.length == 0 {
                        // no data waiting on the bus, we can send new commands
                        debug!(
			    "-> NetMD transmit() writing control message 0x80 with payload: {:02X?}",
			    payload
			);
                        let response = unsafe {
                            libusb_control_transfer(
                                self.usb_device_handle,
                                LIBUSB_ENDPOINT_OUT
                                    | LIBUSB_REQUEST_TYPE_VENDOR
                                    | LIBUSB_RECIPIENT_INTERFACE,
                                0x80,
                                0,
                                0,
                                control_payload_buffer.as_ptr() as *mut c_uchar,
                                control_payload.capacity() as u16,
                                100,
                            )
                        };
                        if response >= 0 {
                            return Ok(response);
                        } else {
                            error!(
                                "NetMD receive() did not respond to write request 0x80: {:?}",
                                LibUSBError::from_i32(response)
                            );
                            return Err(Box::new(LibUSBError::from_i32(response)));
                        }
                    } else {
                        let receive_buffer: &[u8] =
                            &Vec::with_capacity(poll_response.length.into());
                        let transfer_type = LIBUSB_ENDPOINT_IN
                            | LIBUSB_REQUEST_TYPE_VENDOR
                            | LIBUSB_RECIPIENT_INTERFACE;
                        debug!("-> sending control to NetMD device to read data: transfer_type: {:#010b}, command {:02X}, length: {}", transfer_type, poll_response.command, receive_buffer.len());
                        let response = unsafe {
                            libusb_control_transfer(
                                self.usb_device_handle,
                                transfer_type,
                                poll_response.command,
                                0,
                                0,
                                (receive_buffer[..]).as_ptr() as *mut c_uchar,
                                poll_response.length as u16,
                                100,
                            )
                        };
                        if response >= 0 {
                            debug!("read {} bytes of data from NetMD to clear bus before sending command, retrying poll", response);
                            continue;
                        } else {
                            error!("failed to read pending data from NetMD device to clear state during poll: {:?}", LibUSBError::from_i32(response));
                            return Err(Box::new(LibUSBError::from_i32(response)));
                        }
                    }
                }
                Err(e) => {
                    error!("failed to poll NetMD device: {:?}", e);
                    return Err(e);
                }
            }
        }
    }

    /// Uses `transmit()` and then `receive()` to send a new request to the NetMD unit, and then read the results
    ///
    /// The check parameter is used to specify the bytes which should be present int the response if the command was successfully processed, along with an accepted control code.
    /// NetMD devices respond with 6 bytes from the command payload in the response so that the response can be validated against the requesting command.
    /// The actual payload sent to the device is a 0x00 single-byte prefix byte, followed by the check code, and the remainder of the payload. The check code is in reality just
    /// the beginning of the command payload, so the only reason the check code specified separately is to make it easier to compare to the device response in `receive()`.
    pub fn exchange(
        &self,
        check: Vec<u8>,
        payload: Vec<u8>,
    ) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        let control_payload: Vec<u8> = [vec![0x00], check.clone(), payload.clone()].concat();
        debug!(
            "requested NetMD submit() check {:02X?}, payload {:02X?}, combined payload {:02X?}",
            check, payload, control_payload
        );
        match self.poll(1) {
            Ok(_poll_response) => {
                debug!(
                    "NetMD writing control message 0x80 with payload: {:02X?}",
                    payload
                );
                // insert transmit
                match self.transmit(check.clone(), payload) {
                    Ok(_transmit_length) => self.receive(check),
                    Err(e) => Err(e),
                }
            }
            Err(e) => {
                error!("failed to poll NetMD device: {:?}", e);
                Err(e)
            }
        }
    }

    /// Reads and returns any pending data from the NetMD unit
    pub fn receive(&self, check_vec: Vec<u8>) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        debug!("requested NetMD receive(): check value: {:02X?}", check_vec);
        let check: Box<[u8]> = check_vec.clone().into_boxed_slice();
        let mut try_no: i32 = 0;
        'retryloop: loop {
            try_no += 1;
            // this is a bit of safety so we won't retry more than 20 times, but it allows us to
            // also try retries separately in the code below to return the correct error when
            // something like an interim response comes in after too many retries
            if try_no > 20 {
                return Err(Box::new(NetMDResponseTimedOut {}));
            }
            debug!("NetMD polling with poll() during receive()");
            match self.poll(5) {
                Ok(poll_data_waiting) => {
                    debug!(
                        "NetMD poll response during receive(): {:02X?}",
                        poll_data_waiting
                    );
                    let transfer_type = LIBUSB_ENDPOINT_IN
                        | LIBUSB_REQUEST_TYPE_VENDOR
                        | LIBUSB_RECIPIENT_INTERFACE;
                    let mut control_buffer_vec: Vec<u8> =
                        Vec::with_capacity(poll_data_waiting.length.into());
                    // we get a sweet segfault if we don't pre-fill the vec
                    for _i in 0..poll_data_waiting.length {
                        control_buffer_vec.push(0);
                    }
                    // doing this aligns all the values nicely so they behave as a [c_uchar] via FFI
                    let mut control_buffer: Box<[u8]> = control_buffer_vec.into_boxed_slice();

                    debug!(
                        "<- NetMD reading control for poll response with read_control() during receive(): expect {} bytes, transfer_type {:#010b}, command {:02X?}",
                        transfer_type, poll_data_waiting.length, poll_data_waiting.command
                    );
                    // this baffled me for ages: why was this showing as a write (direction=out) in wireshark instead of a read!?
                    // direction out for a control in transfer should be been out of spec
                    // answer: it was defaulting to out because length was zero, which is invalid for a control in message
                    let response = unsafe {
                        libusb_control_transfer(
                            self.usb_device_handle,
                            transfer_type,
                            poll_data_waiting.command,
                            0,
                            0,
                            control_buffer.as_mut_ptr() as *mut c_uchar,
                            poll_data_waiting.length as u16,
                            1000,
                        )
                    };
                    if response >= 0 {
                        let check_length = check_vec.len();
                        debug!(
                            "received NetMD receive() response with size {}, check length {}: {:02X?}",
                            response, check_length, control_buffer
                        );
                        debug!(
                            "validating response against check bytes {:02X?} ({}): {:02X?} ({})",
                            check,
                            check_length,
                            control_buffer,
                            control_buffer.len()
                        );
                        if control_buffer.len() >= check_length + 1 {
                            if control_buffer[1..check_length + 1] == check[0..check_length] {
                                debug!("successfully validated check bits against NetMD response");
                                let control_byte = control_buffer[0];
                                let control_status: ControlStatus =
                                    ControlStatus::from_u8(control_byte);
                                match control_status {
                                    ControlStatus::Accepted => {
                                        return Ok(control_buffer.to_vec());
                                    }
                                    ControlStatus::Interim => {
                                        if try_no > 10 {
                                            return Err(Box::new(NetMDInterimTimedOut {}));
                                        }
                                        debug!(
                                            "retrying NetMD read to interim response code, try {}, data {:?}",
                                            try_no, control_buffer
                                        );
                                        std::thread::sleep(Duration::from_millis(1000));
                                        continue 'retryloop;
                                    }
                                    ControlStatus::Rejected => {
                                        return Err(Box::new(NetMDControlMessageRejected {}))
                                    }
                                    ControlStatus::NotImplemented => {
                                        return Err(Box::new(NetMDControlMessageNotImplemented {}))
                                    }
                                    _ => return Err(Box::new(NetMDInvalidControlResponse)),
                                }
                            } else {
                                // check did not match
                                error!("NetMD check did not match during receive()");
                                return Err(Box::new(NetMDInvalidControlResponse));
                            }
                        } else {
                            // received data was too small
                            error!(
                                "NetMD unit responsed to receive() with less data than reported"
                            );
                            return Err(Box::new(NetMDShortResponse));
                        }
                    } else {
                        error!(
                            "NetMD receive() did not respond to read request {:02X?}: {:?}",
                            poll_data_waiting.command,
                            LibUSBError::from_i32(response)
                        );
                        return Err(Box::new(LibUSBError::from_i32(response)));
                    }
                }
                Err(e) => {
                    error!("NetMD receive() poll failed: {:?}", e);
                    return Err(e);
                }
            }
        }
    }

    /// Polls the NetMD unit and returns the length of data waiting to be sent by the NetMD unit, along with the command used to request the data
    pub fn poll(&self, tries: usize) -> Result<PollResponse, Box<dyn std::error::Error>> {
        debug!("requested NetMD poll(): tries: {}", tries);
        // this works for initialising a compatible buffer
        let mut control_buffer: [u8; 4] = [0; 4];
        for i in 0..tries {
            debug!("<- reading poll data from NetMD unit during poll()");
            let response = unsafe {
                libusb_control_transfer(
                    self.usb_device_handle,
                    LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_INTERFACE,
                    0x01,
                    0,
                    0,
                    // this works
                    control_buffer.as_mut_ptr() as *mut c_uchar,
                    control_buffer.len() as u16,
                    1000,
                )
            };
            if response >= 0 {
                debug!(
                    "received NetMD poll response with size {}: {:02X?}",
                    response, control_buffer
                );
                if control_buffer[0] > 0 {
                    return Ok(PollResponse {
                        length: control_buffer[2],
                        command: control_buffer[1],
                    });
                }
                debug!(
                    "NetMD device does not have bytes to send, polling {} more times",
                    tries - i
                );
                sleep(Duration::from_secs(1));
                continue;
            } else {
                error!(
                    "failed to send control message during NetMD device poll: {:?}",
                    LibUSBError::from_i32(response)
                );
                return Err(Box::new(LibUSBError::from_i32(response)));
            }
        }
        // we've reached out number of poll attempts, so just return the value, even if it's zero
        Ok(PollResponse {
            length: control_buffer[2],
            command: control_buffer[1],
        })
    }

    /// Return the currently recorded duration, the available duration, and total capacity of a minidisc in seconds
    pub fn get_disc_capacity(&self) -> Result<DiscCapacity, Box<dyn std::error::Error>> {
        match self.exchange(
            vec![0x18, 0x06, 0x02, 0x10, 0x10, 0x00],
            vec![0x30, 0x80, 0x03, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00],
        ) {
            Ok(response) => {
                let recorded: u64 =
                    (response[29] as u64 * 3600) + (response[30] as u64 * 60) + response[31] as u64;
                let total: u64 =
                    (response[35] as u64 * 3600) + (response[36] as u64 * 60) + response[37] as u64;
                let available: u64 =
                    (response[42] as u64 * 3600) + (response[43] as u64 * 60) + response[44] as u64;
                return Ok(DiscCapacity {
                    recorded,
                    available,
                    total,
                });
            }
            Err(e) => return Err(e),
        }
    }

    /// Return the current title of the inserted MiniDisc
    ///
    /// This command is used in the `discinfo` example.
    pub fn get_disc_title(&self) -> Result<String, Box<dyn std::error::Error>> {
        match self.exchange(
            vec![0x18, 0x06, 0x02, 0x20, 0x18, 0x01],
            vec![
                0x00, 0x00, 0x30, 0x00, 0x0a, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
            ],
        ) {
            Ok(response) => {
                let title = match std::str::from_utf8(&response[25..response.len()]) {
                    Ok(title) => title,
                    Err(e) => return Err(Box::new(e)),
                };
                Ok(title.to_string())
            }
            Err(e) => Err(e),
        }
    }

    /// Writes a raw title to the inserted MiniDisc
    pub fn set_disc_title(&self, new_title: String) -> Result<(), Box<dyn std::error::Error>> {
        match self.poll(1) {
            Ok(_) => {
                let current_title = match self.get_disc_title() {
                    Ok(current_title) => current_title,
                    Err(e) => return Err(e),
                };
                let current_title_len: u16 = match current_title.len().try_into() {
                    // length of old title
                    Ok(current_title_len) => current_title_len,
                    Err(e) => return Err(Box::new(e)),
                };
                let new_title_len: u16 = match new_title.len().try_into() {
                    // length of new title
                    Ok(new_title_len) => new_title_len,
                    Err(e) => return Err(Box::new(e)),
                };
                let payload_header: &[u8] = &[0x00, 0x00, 0x30, 0x00, 0x0a, 0x00, 0x50, 0x00];
                let current_title_len_u8 = &current_title_len.to_be_bytes();
                let new_title_len_u8 = &new_title_len.to_be_bytes();
                let new_title_bytes = new_title.as_bytes();
                let payload = [
                    payload_header,
                    new_title_len_u8,
                    &[0x00, 0x00],
                    current_title_len_u8,
                    new_title_bytes,
                ]
                .concat();
                // send the first three handshakes first, the last will be sent after the actual payload
                match self.send_handshakes(&handshakes::HANDSHAKE_SET_DISC_TITLE[0..3]) {
                    Ok(_) => {}
                    Err(e) => {
                        error!(
                            "failed to send special device handshake for HANDSHAKE_SET_DISC_TITLE: {}",
                            e
                        );
                        return Err(e);
                    }
                }
                match self.exchange(vec![0x18, 0x07, 0x02, 0x20, 0x18, 0x01], payload) {
                    Ok(_) => (),
                    Err(e) => return Err(e),
                }
                // send the last handshake
                match self.send_handshakes(&handshakes::HANDSHAKE_SET_DISC_TITLE[4..4]) {
                    Ok(_) => {}
                    Err(e) => {
                        error!(
                            "failed to send special device handshake for HANDSHAKE_SET_DISC_TITLE: {}",
                            e
                        );
                        return Err(e);
                    }
                }
                match self.exchange(vec![0x18, 0x08, 0x10, 0x18, 0x01, 0x00], vec![0x00]) {
                    Ok(_) => Ok(()),
                    Err(e) => return Err(e),
                }
            }
            Err(e) => Err(e),
        }
    }

    /// Sends the priovided handshake via the exchange() function, for each handshake in the list
    ///
    /// This command is used by several privileged commands on some units, such as the JE780, to enable functionality.
    /// It is not necessary but seemingly harmless on other units that don't require the handshakes be sent prior to
    /// privileged requests being sent.
    pub fn send_handshakes(
        &self,
        handshakes: &[[u8; 6]],
    ) -> Result<(), Box<dyn std::error::Error>> {
        for handshake in handshakes {
            // handshakes are all a sequence of 8 byte commands.
            // they follow the same format as commands, however the trailing byte is always a single 0x0 after
            // the portion of the command payload which is also returned in the reply - the "check" payload.
            // so, we can reliably use the below pattern to exchange handshakes with the unit.
            match self.exchange(handshake.to_vec(), vec![0x00]) {
                Ok(_) => {
                    debug!(
                        "successfully exchanged special handshake {:02X?} with the unit",
                        handshake
                    );
                }
                Err(e) => {
                    error!(
                        "failed to exchange special handshake {:02X?} with the unit: {}",
                        handshake, e
                    );
                    return Err(e);
                }
            }
        }
        Ok(())
    }

    /// Returns the current default recording parameters (mode and audio channels) for the connected NetMD unit
    ///
    /// This command is used in the `device_info` example
    pub fn get_recording_parameters(
        &self,
    ) -> Result<RecordingParameters, Box<dyn std::error::Error>> {
        match self.send_handshakes(&handshakes::HANDSHAKE_GET_STATUS) {
            Ok(_) => {}
            Err(e) => {
                error!(
                    "failed to send special device handshake for HANDSHAKE_GET_STATUS: {}",
                    e
                );
                return Err(e);
            }
        }
        match self.exchange(
            vec![0x18, 0x09, 0x80, 0x01, 0x03, 0x30],
            vec![
                0x88, 0x01, 0x00, 0x30, 0x88, 0x05, 0x00, 0x30, 0x88, 0x07, 0x00, 0xff, 0x00, 0x00,
                0x00, 0x00, 0x00,
            ],
        ) {
            Ok(response) => Ok(RecordingParameters {
                encoding: Encoding::from_u8(response[34]),
                channels: Channels::from_u8(response[35]),
            }),
            Err(e) => return Err(e),
        }
    }

    /// Returns known status flags (disc loaded, no disc) for the connected NetMD unit
    ///
    /// This command is used in the `device_info` example
    pub fn get_status(&self) -> Result<Status, Box<dyn std::error::Error>> {
        match self.send_handshakes(&handshakes::HANDSHAKE_GET_STATUS) {
            Ok(_) => {}
            Err(e) => {
                error!(
                    "failed to send special device handshake for HANDSHAKE_GET_STATUS: {}",
                    e
                );
                return Err(e);
            }
        }
        match self.exchange(
            vec![0x18, 0x09, 0x80, 0x01, 0x02, 0x30],
            vec![
                0x88, 0x00, 0x00, 0x30, 0x88, 0x04, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
            ],
        ) {
            Ok(response) => Ok(Status::from_u8(response[26])),
            Err(e) => return Err(e),
        }
    }

    /// Returns the number of tracks on the currently inserted MiniDisc
    ///
    /// This command is used in the `discinfo` example.
    pub fn get_track_count(&self) -> Result<u16, Box<dyn std::error::Error>> {
        debug!("requested NetMD get_track_count()");
        match self.exchange(vec![0x18, 0x08, 0x10, 0x10, 0x01, 0x01], vec![0x00]) {
            Ok(_) => (),
            Err(e) => return Err(e),
        }
        match self.exchange(
            vec![0x18, 0x06, 0x02, 0x10, 0x10, 0x01],
            vec![0x30, 0x00, 0x10, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00],
        ) {
            Ok(response) => {
                debug!(
                    "get_track_count(): got raw bytes from NetMD unit {:02X?}",
                    response
                );
                let response_bytes: &[u8] = &response[23..25];
                debug!(
                    "get_track_count(): got track bytes from NetMD unit {:02X?}",
                    response_bytes
                );
                Ok(u8_array_to_u16(response_bytes))
            }
            Err(e) => return Err(e),
        }
    }

    /// Returns the track title at the provided zero-indexed offset on the currently inserted MiniDisc
    ///
    /// This command is used in the `discinfo` example.
    pub fn get_track_title(&self, track: u16) -> Result<String, Box<dyn std::error::Error>> {
        let track_bytes = track.to_be_bytes();
        match self.exchange(
            vec![0x18, 0x06, 0x02, 0x20, 0x18, 0x02],
            vec![
                track_bytes[0],
                track_bytes[1],
                0x30,
                0x00,
                0x0a,
                0x00,
                0xff,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
            ],
        ) {
            Ok(response) => {
                let title = match std::str::from_utf8(&response[25..response.len()]) {
                    Ok(title) => title,
                    Err(e) => return Err(Box::new(e)),
                };
                Ok(title.to_string())
            }
            Err(e) => return Err(e),
        }
    }

    // set_track_title sets the title of the track at the provided zero-indexed track offset, new indicates the track is newly added
    pub fn set_track_title(
        &self,
        track: u16,
        title: String,
        new: bool,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let track_bytes = track.to_be_bytes();
        let old_track_title_length: u16 = match new {
            true => 0,
            false => match self.get_track_title(track) {
                Ok(old_track_title) => match old_track_title.len().try_into() {
                    Ok(old_track_title_length) => old_track_title_length,
                    Err(e) => return Err(Box::new(e)),
                },
                Err(e) => return Err(e),
            },
        };
        let new_track_title_length: u16 = match title.len().try_into() {
            Ok(new_track_title_length) => new_track_title_length,
            Err(e) => return Err(Box::new(e)),
        };

        if !new {
            match self.exchange(vec![0x18, 0x08, 0x10, 0x18, 0x02, 0x00], vec![0x00]) {
                Ok(_) => (),
                Err(e) => return Err(e),
            }
            match self.exchange(vec![0x18, 0x08, 0x10, 0x18, 0x02, 0x03], vec![0x00]) {
                Ok(_) => (),
                Err(e) => return Err(e),
            }
        }

        let new_track_title_length_bytes = new_track_title_length.to_be_bytes();
        let old_track_title_length_bytes = old_track_title_length.to_be_bytes();

        match self.exchange(
            vec![0x18, 0x07, 0x02, 0x20, 0x18, 0x02],
            [
                vec![
                    track_bytes[0],
                    track_bytes[1],
                    0x30,
                    0x00,
                    0x0a,
                    0x00,
                    0x50,
                    0x00,
                ],
                new_track_title_length_bytes.to_vec(),
                [0x00, 0x00].to_vec(),
                old_track_title_length_bytes.to_vec(),
                title.as_bytes().to_vec(),
            ]
            .concat(),
        ) {
            Ok(_) => (),
            Err(e) => return Err(e),
        }

        if !new {
            match self.exchange(vec![0x18, 0x08, 0x10, 0x18, 0x02, 0x00], vec![0x00]) {
                Ok(_) => (),
                Err(e) => return Err(e),
            }
        }
        Ok(())
    }

    /// Returns the known track flags (such as protection mode) for the provided zero-indexed track number on the currently inserted MiniDisc
    ///
    /// This command is used in the `discinfo` example.
    pub fn get_track_flags(&self, track: u16) -> Result<TrackFlag, Box<dyn std::error::Error>> {
        let track_bytes = track.to_be_bytes();
        // does track need to be 2x u8's to represent an u16 here? it hasn't in other commands?
        match self.exchange(
            vec![0x18, 0x06],
            vec![
                0x01,
                0x20,
                0x10,
                0x01,
                track_bytes[0],
                track_bytes[1],
                0xff,
                0x00,
                0x00,
                0x01,
                0x00,
                0x08,
            ],
        ) {
            Ok(response) => Ok(TrackFlag::from_u8(response[15])),
            Err(e) => return Err(e),
        }
    }

    // erase_track will erase the track at the provided zero-indexed offset
    pub fn erase_track(&self, track: u8) -> Result<(), Box<dyn std::error::Error>> {
        match self.exchange(
            vec![0x18, 0x40],
            vec![0xff, 0x01, 0x00, 0x20, 0x10, 0x01, track],
        ) {
            Ok(_) => Ok(()),
            Err(e) => return Err(e),
        }
    }

    // move_track will move the track at the provided zero-indexed offset to a new zero-indexed track offset on the disc
    pub fn move_track(&self, track: u8, new_track: u8) -> Result<(), Box<dyn std::error::Error>> {
        match self.exchange(vec![0x18, 0x08, 0x10, 0x10, 0x01, 0x00], vec![0x00]) {
            Ok(_) => (),
            Err(e) => return Err(e),
        };
        match self.exchange(
            vec![0x18, 0x43],
            vec![
                0xff, 0x00, 0x00, 0x20, 0x10, 0x01, track, 0x20, 0x10, 0x01, new_track,
            ],
        ) {
            Ok(_) => Ok(()),
            Err(e) => return Err(e),
        }
    }

    /// Returns the length of the provided zero-indexed track in seconds for the currently inserted MiniDisc
    ///
    /// This command is used in the `discinfo` example.
    pub fn get_track_length(&self, track: u16) -> Result<Duration, Box<dyn std::error::Error>> {
        let track_bytes = track.to_be_bytes();
        match self.exchange(
            vec![0x18, 0x06],
            vec![
                0x02,
                0x20,
                0x10,
                0x01,
                track_bytes[0],
                track_bytes[1],
                0x30,
                0x00,
                0x01,
                0x00,
                0xff,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
            ],
        ) {
            Ok(response) => {
                let track_duration = match hex_timestamp_to_duration(&response[27..30]) {
                    Ok(track_duration) => track_duration,
                    Err(e) => return Err(e),
                };
                debug!(
                    "raw NetMD track duration for track {} is {:02X?} {:02X?} {:02X?} ({:?})",
                    track, response[27], response[28], response[29], track_duration
                );
                Ok(track_duration)
            }
            Err(e) => return Err(e),
        }
    }

    /// Returns the encoding format (SP, LP2, LP4) for the provided zero-indexed track number on the currently inserted MiniDisc
    ///
    /// This command is used in the `discinfo` example.
    pub fn get_track_encoding(&self, track: u16) -> Result<Encoding, Box<dyn std::error::Error>> {
        let track_bytes = track.to_be_bytes();
        match self.exchange(
            vec![0x18, 0x06, 0x02, 0x20, 0x10, 0x01],
            vec![
                track_bytes[0],
                track_bytes[1],
                0x30,
                0x80,
                0x07,
                0x00,
                0xff,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
            ],
        ) {
            Ok(response) => Ok(Encoding::from_u8(response[response.len() - 2])),
            Err(e) => return Err(e),
        }
    }
}
