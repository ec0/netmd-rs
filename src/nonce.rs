use rand::Rng;

pub struct Nonce {
    pub host: [u8; 8],
    pub dev: [u8; 8],
}

pub fn new_nonce() -> Result<Nonce, Box<dyn std::error::Error>> {
    let mut n: Nonce = Nonce {
        host: [0; 8],
        dev: [0; 8],
    };
    for i in 0..8 {
        n.host[i] = rand::thread_rng().gen::<u8>() & 0xff;
    }
    return Ok(n);
}

impl Nonce {
    // merge the host and dev nonces
    pub fn merged(&self) -> [u8; 16] {
        let mut merged: [u8; 16] = [0; 16];
        let concat = [self.host, self.dev].concat();
        // I wonder if there's a way to avoid this iteration
        for i in 0..merged.len() - 1 {
            merged[i] = concat[i];
        }
        return merged;
    }
}
