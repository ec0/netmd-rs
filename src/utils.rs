use byteorder::{BigEndian, ByteOrder};
use log::debug;
use std::time::Duration;

use crate::errors::NetMDInvalidTimeStamp;

pub fn u8_array_to_u16(num: &[u8]) -> u16 {
    BigEndian::read_u16(num)
}

fn hex_byte_to_literal_int(data: u8) -> Result<u64, Box<dyn std::error::Error>> {
    // get u8 as hex
    let data_str: String = format!("{:X}", data);
    // parse hex as int
    let data_num: u64 = match data_str.parse() {
        Ok(data_num) => data_num,
        Err(e) => return Err(Box::new(e)),
    };
    debug!("parsed NetMD timestamp byte {:02X} into {}", data, data_num);
    Ok(data_num)
}

pub fn hex_timestamp_to_duration(data: &[u8]) -> Result<Duration, Box<dyn std::error::Error>> {
    debug!("parsing NetMD timestamp from {:?}", data);
    if data.len() == 3 {
        // hours
        let hours: u64 = match hex_byte_to_literal_int(data[0]) {
            Ok(hours) => hours,
            Err(e) => return Err(e),
        };
        // minutes
        let minutes: u64 = match hex_byte_to_literal_int(data[1]) {
            Ok(minutes) => minutes,
            Err(e) => return Err(e),
        };
        // seconds
        let seconds: u64 = match hex_byte_to_literal_int(data[2]) {
            Ok(seconds) => seconds,
            Err(e) => return Err(e),
        };
        return Ok(Duration::from_secs(
            (((hours * 60) + minutes) * 60) + seconds,
        ));
    }
    return Err(Box::new(NetMDInvalidTimeStamp));
}
