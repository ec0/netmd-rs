extern crate libc;
extern crate libusb_sys as ffi;

#[derive(Debug, Clone)]
pub struct NetMDDevice {
    pub vendor_id: u16,
    pub product_id: u16,
    pub name: &'static str,
}

pub const NETMD_DEVICES: [NetMDDevice; 40] = [
    NetMDDevice {
        vendor_id: 0x04dd,
        product_id: 0x7202,
        name: "Sharp IM-MT899H",
    },
    NetMDDevice {
        vendor_id: 0x04dd,
        product_id: 0x9013,
        name: "Sharp IM-DR400/DR410/DR420",
    },
    NetMDDevice {
        vendor_id: 0x04dd,
        product_id: 0x9014,
        name: "Sharp IM-DR80",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0034,
        name: "Sony PCLK-XX",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0036,
        name: "Sony",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0075,
        name: "Sony MZ-N1",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x007c,
        name: "Sony",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0080,
        name: "Sony LAM-1",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0081,
        name: "Sony MDS-JB980/JE780",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0084,
        name: "Sony MZ-N505",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0085,
        name: "Sony MZ-S1",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0086,
        name: "Sony MZ-N707",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x008e,
        name: "Sony CMT-C7NT",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0097,
        name: "Sony PCGA-MDN1",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x00ad,
        name: "Sony CMT-L7HD",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x00c6,
        name: "Sony MZ-N10",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x00c7,
        name: "Sony MZ-N910",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x00c8,
        name: "Sony MZ-N710/NF810",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x00c9,
        name: "Sony MZ-N510/N610",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x00ca,
        name: "Sony MZ-NE410/NF520D",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x00eb,
        name: "Sony MZ-NE810/NE910",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0101,
        name: "Sony LAM-10",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0113,
        name: "Aiwa AM-NX1",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x013f,
        name: "Sony MDS-S500",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x014c,
        name: "Aiwa AM-NX9",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x017e,
        name: "Sony MZ-NH1",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0180,
        name: "Sony MZ-NH3D",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0182,
        name: "Sony MZ-NH900",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0184,
        name: "Sony MZ-NH700/NH800",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0186,
        name: "Sony MZ-NH600",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0187,
        name: "Sony MZ-NH600D",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0188,
        name: "Sony MZ-N920",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x018a,
        name: "Sony LAM-3",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x01e9,
        name: "Sony MZ-DH10P",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0219,
        name: "Sony MZ-RH10",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x021b,
        name: "Sony MZ-RH710/MZ-RH910",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x021d,
        name: "Sony CMT-AH10",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x022c,
        name: "Sony CMT-AH10",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x023c,
        name: "Sony DS-HMD1",
    },
    NetMDDevice {
        vendor_id: 0x054c,
        product_id: 0x0286,
        name: "Sony MZ-RH1",
    },
];
