netmd-rs
--------

netmd-rs is a crate for accessing MiniDisc recorders which support the NetMD protocol, for interfacing MiniDisc equipment with computers via USB.

features
--------
* device detection
* multiple device handling (you can use an index to choose the target device)
* disc information (duration, title, track count)
* track information (title, duration, flags)
* basic untested crypto support for transfers
* netmd timestamp parsing to rust std::time::Duration

TODO
----
* group support
* disc status
* erasing tracks
* erasing discs
* appending tracks
* send support (audio transfers)

unsafe code
-----------

this crate uses libusb-sys directly as an FFI binding to perform USB operations. this means, there is unsafe code here. 
in the future, I intend to move back to using rusb or similar safe bindings to libusb, however the level of control using libusb-sys gives me has advantages, so until it starts crashing...

credits
-------

this project draws heavily from the following projects & sources of information -
* https://github.com/enimatek-nl/go-netmd-lib
* https://github.com/linux-minidisc/linux-minidisc

author
------
James / echo
email: james <at> tachibana.systems
fedi: @james@spooky.computer
