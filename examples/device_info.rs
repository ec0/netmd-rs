use env_logger::{self, Env};
use log::{error, info};
use netmd;

fn main() {
    let env = Env::default().filter_or("RUST_LOG", "info");
    env_logger::init_from_env(env);
    let netmd: netmd::NetMD = match netmd::new_netmd(0) {
        Ok(netmd) => netmd,
        Err(e) => {
            error!("failed to open NetMD device at index 0: {:?}", e);
            return;
        }
    };
    info!("detected NetMD device {}", netmd.device_name);
    match netmd.get_recording_parameters() {
        Ok(parameters) => {
            info!("NetMD unit recording parameters: {}", parameters);
        }
        Err(e) => {
            error!("failed to get recording parameters: {:?}", e);
        }
    };
    match netmd.get_status() {
        Ok(status) => {
            info!("NetMD unit status: {}", status);
        }
        Err(e) => {
            error!("failed to get unit status: {:?}", e);
        }
    };
}
