use env_logger::{self, Env};
use log::{error, info};
use netmd;
use std::env;

fn main() {
    let env = Env::default().filter_or("RUST_LOG", "info");
    env_logger::init_from_env(env);
    let netmd: netmd::NetMD = match netmd::new_netmd(0) {
        Ok(netmd) => netmd,
        Err(e) => {
            error!("failed to open NetMD device at index 0: {:?}", e);
            return;
        }
    };
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        error!("please provide a new disc title as the first argument to this example");
        return;
    }
    let new_disc_title: String = args[1].clone();
    info!("desired new disc title: {}", new_disc_title);
    let old_disc_title: String = match netmd.get_disc_title() {
        Ok(disc_title) => disc_title,
        Err(e) => {
            error!("failed to get current disc title: {}", e);
            return;
        }
    };
    info!("current disc title: {}", old_disc_title);
    match netmd.set_disc_title(new_disc_title.clone()) {
        Ok(_) => {
            info!("set disc title: {}", new_disc_title);
            let validated_disc_title: String = match netmd.get_disc_title() {
                Ok(disc_title) => disc_title,
                Err(e) => {
                    error!("failed to get updated disc title: {}", e);
                    return;
                }
            };
            if validated_disc_title == new_disc_title {
                info!("successfully set disc title");
            } else {
                error!(
                    "validation failed - requested title: {}, actual title: {}",
                    new_disc_title, validated_disc_title
                );
            }
        }
        Err(e) => {
            error!("failed to set disc title: {}", e);
        }
    }
}
