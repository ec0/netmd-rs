use env_logger::{self, Env};
use hhmmss::Hhmmss;
use log::{debug, error, info};
use netmd;

fn main() {
    let env = Env::default().filter_or("RUST_LOG", "info");
    env_logger::init_from_env(env);
    let netmd: netmd::NetMD = match netmd::new_netmd(0) {
        Ok(netmd) => netmd,
        Err(e) => {
            error!("failed to open NetMD device at index 0: {:?}", e);
            return;
        }
    };
    let _disc_title: String = match netmd.get_disc_title() {
        Ok(disc_title) => {
            info!("disc title: {}", disc_title);
            disc_title
        }
        Err(e) => {
            error!("failed to list tracks: {:?}", e);
            return;
        }
    };
    let track_count: u16 = match netmd.get_track_count() {
        Ok(track_count) => {
            info!("track count: {}", track_count);
            track_count
        }
        Err(e) => {
            error!("failed to list tracks: {:?}", e);
            return;
        }
    };
    for track_no in 0..track_count {
        debug!("getting details for track {}", track_no);
        let track_title = match netmd.get_track_title(track_no) {
            Ok(track_title) => track_title,
            Err(e) => {
                error!("failed to get title for track {}: {}", track_no, e);
                return;
            }
        };
        let track_length: std::time::Duration = match netmd.get_track_length(track_no) {
            Ok(track_length) => track_length,
            Err(e) => {
                error!("failed to get track length for track {}: {}", track_no, e);
                return;
            }
        };
        let track_encoding = match netmd.get_track_encoding(track_no) {
            Ok(track_encoding) => track_encoding,
            Err(e) => {
                error!(
                    "failed to get track recording encoding for track {}: {}",
                    track_no, e
                );
                return;
            }
        };
        let track_flags = match netmd.get_track_flags(track_no) {
            Ok(track_flags) => track_flags,
            Err(e) => {
                error!("failed to get track flags for track {}: {}", track_no, e);
                return;
            }
        };
        info!(
            "{}: {} ({}) [{}, {}]",
            track_no + 1,
            track_title,
            track_length.hhmmss(),
            track_encoding,
            track_flags,
        );
    }
}
